﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;


namespace WinFormswithEFSample
{
    public class ProductContext : DbContext
    {
        public ProductContext() : base(nameOrConnectionString:
            "myConnection") { }
        public DbSet<Category> Categories { get; set; }        public DbSet<Product> Products { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("public");
            base.OnModelCreating(modelBuilder);
        }






    }
}
